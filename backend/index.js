const express = require('express');
const app = express()
const bp = require('body-parser')

data = []

app.get('/', (req, res) => {
    res.send("The server is up and running!")
})

app.get('/data', (req, res) => {
    res.send(data)
})

app.use(express.json())
app.use(bp.urlencoded({ extended: true }))

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

app.post('/input', (req, res) => {
    var userInput = req.body["userIn"];
    data.push(userInput)
    console.log(userInput)
    res.send("Successfully added " + userInput)
})

const port = process.env.PORT || 3000;

app.listen(3000, () => {
    console.log("The server is running on port 3000!")
})