var information = httpGet("http://localhost:3000/data")
var converted_information = JSON.parse(information)

function httpGet(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", theUrl, false); // false for synchronous request
    xmlHttp.send(null);
    return xmlHttp.responseText;
}

function populate(data) {
    let board = document.querySelector(".board");
    let n = data.length;
    for (let i = 0; i < n; i++) {
        let post = document.createElement("div");
        post.innerHTML = data[i];
        post.classList.add("posts");
        board.insertAdjacentElement("beforeend", post);
    }
}

populate(converted_information)