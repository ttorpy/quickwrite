const { response } = require("express");

function update() {
    var userInput = document.getElementById("userInput").value;
    console.log(userInput)
    inHandler(userInput)
}

async function inHandler(userInput) {
    try {
        var response = await fetch('http://localhost:3000/input', {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "userIn": userInput
            })
        });
    } catch (error) {
        console.log("failed");
    }
}


/*async function inHandler(userInput) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:3000/input", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify({
        "userIn": userInput
    }));

    xhr.onreadystatechange = function() {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            var jsonResponse = JSON.parse(xhr.responseText)
            window.location.replace(jsonResponse.redirectRoute)
        }
    }
}*/