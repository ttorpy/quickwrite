# QuickWrite

This is a fullstack app with the frontend and the backend where you can quickly write stuff and post it to the output site.

The output is fetched and displayed from the server.

Utilizes technical knowledge in NodeJs and ExpressJS 

Please download the source code if you want to run this

Instructions for running
1. Start the back end server. Use nodeJS to run the backend server files.
2. Start the front end server. Use your local brower to open up the input and output files in js